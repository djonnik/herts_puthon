def get_symbol_text(text: str) -> dict:
    rez_dict = {}
    text_long = len(text)
    for symbol in set(text):
        rez_dict[symbol] = text.count(symbol) / text_long * 100
    return rez_dict


if __name__ == "__main__":
    print(get_symbol_text(text=input("Input text: ")))
