def get_ternarne(x: int, y: int) -> str:
    return str(
        x + y
        if (x < y)
        else x - y
        if (x > y)
        else "game over"
        if (x == 0 and y == 0)
        else 0
    )


if __name__ == "__main__":
    x = input("Inpun X: ")
    y = input("Inpun Y: ")
    print(get_ternarne(int(x), int(y)))

#     casses = (
#         (1, 2, "3"),
#         (1, 1, "0"),
#         (2, 1, "1"),
#        (0, 0, "game over"),
#     )
#      for x, y, rezult in casses:
#          run_get_ternare = get_ternarne(x, y)
#          assert run_get_ternare == rezult, f"ERROR: get_ternare{x}, {y} retupned {run_get_ternare}, expected {rezult}"
